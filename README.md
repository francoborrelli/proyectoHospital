# Proyecto de Software 2017

## Bot de Turnos - Telegram
[@HospitalGutierrez_Bot](https://telegram.me/HospitalGutierrez_Bot)

## Introducción

Trabajo final para la meteria Proyecto de Software. 

Este es un sistema para la administración del hospital Gutierrez de la ciudad de La Plata. El mismo provee manejo para el control de usuarios, pacientes (datos personales, datos demograficos, historia clinica)

## Documentación

Se adjunta en formato de pdf la documentación relacionada al proyecto desarrollado. 

## Iniciar Aplicación

El desarrollo se encuentra dentro de la carpeta "final" de acuerdo a lo especificado.

Tanto en la carpeta "client" como en "server" cuentan con su propio archivo Readme.
En cada uno de ellos se explica como instalar las dependencias del proyecto y como inicializarlo.

## Cuentas de prueba

* Cuenta Administrador
  * Email: admin@gmail.com
  * Contraseña: 123456

* Cuenta Pediatra
  * Email: pediatra@gmail.com
  * Contraseña: 123456

* Cuenta Recepcionista
  * Email: recepcionista@gmail.com
  * Contraseña: 123456